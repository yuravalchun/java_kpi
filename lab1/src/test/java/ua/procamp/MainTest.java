package ua.procamp;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
//import org.junit.Test;

public class MainTest {
    @Test
    public void solutionTest() throws IllegalArgumentException {
        Main obj = new Main();
        obj.solution(1, -2, -3);
        Assert.assertEquals(3.0 , obj.x[0], 0.0000001);
        Assert.assertEquals(-1.0 , obj.x[1], 0.0000001);
    }
    @Test
    public void solutionTest2() throws IllegalArgumentException {
        Main obj = new Main();
        obj.solution(0, -2, -3);
        Assert.assertEquals(-1.5 , obj.x[0], 0.0000001);
    }

    @Test
    public void solutionTest3() throws IllegalArgumentException {
        Main obj = new Main();
        try {
            obj.solution(0, 0, 0);
            Assert.fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }
    }

    @Test
    public void solutionTest4() throws IllegalArgumentException {
        Main obj = new Main();
        try {
            obj.solution(1, 1, 3);
            Assert.fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }
    }

//    @Test
//    public void solutionTest5() throws IllegalArgumentException {
//        Main obj = new Main();
//        try {
//            obj.solution(1, 1, 3);
//            Assert.fail("Expected IllegalArgumentException");
//        } catch (IllegalArgumentException thrown) {
//            Assert.assertNotEquals("", thrown.getMessage());
//        }
//    }
}
