package ua.procamp;

import java.util.Scanner;

public class Main {

    private static String message = "error";
    public static double[] x;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double a, b, c;
        System.out.println("Введите а: ");
        a = in.nextDouble();
        System.out.println("Введите b: ");
        b = in.nextDouble();
        System.out.println("Введите c: ");
        c = in.nextDouble();
        if (solution(a, b, c)) {
            System.out.printf("Решения уравнения:\n");
            if (x.length != 1) {
                for (int i = 0; i < x.length; i++) {
                    System.out.printf("x" + (i + 1) + " = " + x[i] + "\n");
                }
            } else System.out.printf("x = " + x[0]);
        } else System.out.print(message);
    }

    public static boolean solution(double a, double b, double c) {
        if (a == b && b==c && c == 0){
            message = "Решение уравнения:\nx є R";
                throw new IllegalArgumentException(message);
        }
        double d;
        d = Math.pow(b, 2) - (4 * a * c);
        if (d > 0) {
            if (a != 0) {
                x = new double[2];
                x[0] = ((-1) * b + Math.sqrt(d)) / (2 * a);
                x[1] = ((-1) * b - Math.sqrt(d)) / (2 * a);
            } else {
                x = new double[1];
                if (c !=0) {
                    x[0] = (-1) * c / b;
                } else {
                    x[0] = 0;
                }
            }
            return true;
        } else if (d == 0) {
            x = new double[1];
            x[0] = (-1) * b / (2 * a);
            return true;
        } else {
            message = "Ошибка, D < 0";
                throw new IllegalArgumentException(message);
        }
    }
}