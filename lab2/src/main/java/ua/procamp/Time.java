package ua.procamp;

import java.util.Date;

public class Time {
    protected Date time;

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    protected Time(Date time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Time{" +
                "time=" + time +
                '}';
    }
}
