package ua.procamp;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Timetable extends Time {
    private String discipline;
    private int audience;

    public Timetable(Date time, String discipline, int audience) {
        super(time);
        this.discipline = discipline;
        this.audience = audience;
    }

    public String getDiscipline() {
        return discipline;
    }

    @Command(name = "Задати дисціпліну",
            args = "Дисціпліна ",
            desc = "Задає назву дисціпліни")
    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public int getAudience() {
        return audience;
    }

    public void setAudience(int audience) {
        this.audience = audience;
    }

    @Command(name = "Конвертація в строку",
            args = "",
            desc = "Повертає список всіх аргументів в виді строки ")
    @Override
    public String toString() {
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("hh:mm:ss");
        return "Timetable{" +
                "discipline='" + discipline + '\'' +
                ", audience='" + audience + '\'' +
                 ", time=" +  formatForDateNow.format(time) +
                '}' ;
    }

    @Command(name = "Час до заняття",
            args = "",
            desc = "Повертає час від цього моменту до початку заняття в форматі hh:mm:ss")
    public String timeToLesson() {
        Date thisTime = new Date();
        Date result = new Date(time.getTime() - thisTime.getTime());
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("hh:mm:ss");
        return formatForDateNow.format(result);
    }
}
