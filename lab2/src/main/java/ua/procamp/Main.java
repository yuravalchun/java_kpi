package ua.procamp;

import java.lang.reflect.Method;
import java.util.Date;
import java.lang.reflect.InvocationTargetException;

public class Main {

    public static void main(String[] args) throws InvocationTargetException {
        Date thisTime = new Date();
        Timetable obj = new Timetable(new Date(thisTime.getTime() + 10000), "Java", 4);
        System.out.println(obj.timeToLesson());
        StringBuilder sb = new StringBuilder("Завдання №1.\n Список методів: \n");
        for (Method m : Timetable.class.getDeclaredMethods())
        {
            if (m.isAnnotationPresent(Command.class))
            {
                Command com = m.getAnnotation(Command.class);

                sb.append("#Метод " + m.getName()).append(" | Ім'я:" + com.name()).append(" | ").append("Параметри:" + com.args()).append(" | ").append("Опис:" + com.desc()).append("\n");
                Class[] paramTypes = m.getParameterTypes();
                if (paramTypes.length > 0) {
                    sb.append("Типи параметрів: ");
                    for (Class paramType : paramTypes) {
                        sb.append(" " + paramType.getName());
                    }
                    sb.append("\n");
                }
            }
        }
        System.out.println(sb.toString());

        sb = new StringBuilder("Завдання №2.\n");
        System.out.println("Назва пакету: " + Timetable.class.getPackage());
        System.out.println("Назва класу: " + Timetable.class.getSimpleName());
        try {
            for (Method m : obj.getClass().getDeclaredMethods()) {
                if (m.isAnnotationPresent(Command.class) && m.getParameterTypes().length == 0) {
                    Object  result =  m.invoke(obj);
                    sb.append(result);
                    sb.append("\n");
                }
            }
            System.out.println(sb.toString());
        }
          catch (IllegalAccessException e){
              e.printStackTrace();
          }


    }
}
